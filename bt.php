<!DOCTYPE html>

<head>
  <title>My Website</title>
  <link rel="stylesheet" href="index.css">
</head>

<body>
  <div class="ms">
    <div class="container">
      <ul>
        <li><a href="https://www.instagram.com/alhadidsf_"><img src="ig.png" width="25px" height="25px"></a></li>
        <li><a href="https://twitter.com/sufe_afaral"><img src="twit.png" width="25px" height="25px"></a></li>
        <li><a href="whatsapp://send?text=&phone=+6285297902614"><img src="wa.png" width="25px" height="25px"></a></li>
      </ul>
    </div>
  </div>
  <header>


    <div class="container">
      <h1><a href="index.html">ALHADID SUFA PURNAMA</a></h1>
      <ul>
        <li><a href="index.html">HOME</a></li>
        <li><a href="about.html">ABOUT</a></li>
        <li class="active"><a href="bt.php">BUKU TAMU</a></li>
        <li><a href="contact.html">CONTACT</a></li>
      </ul>
    </div>
  </header>


  <section class="label">
    <div class="container">
      <p>Home / Buku Tamu</p>
    </div>
  </section>

  <section class="bt1">
    <div class="container">
      <h3>BUKU TAMU</h3>
    </div>
  </section>
  <div class="bt">
    <form id="form1" name="form1" method="post" action="proses.php">
      <table width="58%" border="0" align="center">
        <tr>
          <td>Nama Lengkap</td>
          <td><input name="nama" type="text" id="nama"></td>
        </tr>
        <tr>
          <td>Alamat</td>
          <td><input name="alamat" type="text" id="alamat"></td>
        </tr>
        <tr>
          <td>E-Mail</td>
          <td><input name="email" type="text" id="email"></td>
        </tr>
        </tr>
        <tr>
          <td>Status</td>
          <td><select name="status" id="status">
              <option>Menikah</option>
              <option>Belum Menikah</option>
            </select></td>
        </tr>
        <tr>
          <td>Komentar</td>
          <td><textarea name="komentar" id="komentar"></textarea></td>
          </textarea>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><input class="submit" type="submit" name="Submit" value="Kirim"><input class="submit" type="reset"
              name="Submit2" value="Batal">
          </td>
        </tr>
      </table>
    </form>
  </div>
  <div class="footer">
    <footer>
      <p class="copy">Copyright 2021 &copy; AlhadidSufaPurnama. All Right Reserved.</p>
    </footer>
  </div>
</body>

</html>